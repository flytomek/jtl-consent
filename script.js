function giveConsentGC(detail) {
    var update = false;
    if (detail !== null && typeof detail.gc_consent_google_tagmanager !== 'undefined') {
        CheckConsentGoogle(detail, 'google_tagmanager');
    }
    if (detail !== null && typeof detail.gc_consent_analytics !== 'undefined') {
        CheckConsentGoogle(detail, 'analytics');
    }
    if (detail !== null && typeof detail.gc_consent_adwords !== 'undefined') {
        CheckConsentGoogle(detail, 'adwords');
    }

    if (detail !== null && typeof detail.gc_analytics !== 'undefined') {
        if (detail.gc_analytics === true) {
            sendConsentToAnalytics('grant');
        } else {
            sendConsentToAnalytics('revoke');
        }
        update = true;
    }

    if (detail !== null && typeof detail.gc_adwords !== 'undefined') {
        if (detail.gc_adwords === true) {
            sendConsentToAdwords('grant');
        } else {
            sendConsentToAdwords('revoke');
        }
        var update = true;
    }

    if (update) {
        updateConsent();
    }

}

function updateConsent() {
    if (typeof gtag !== 'undefined') {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'event': 'update_consent',
        });
    }
}